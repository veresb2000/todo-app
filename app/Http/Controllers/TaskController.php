<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class TaskController extends Controller
{
    public function index()
    {
        $categories = Category::with(['tasks' => function ($query) {
            $query->where('user_id', auth()->user()->id);
        }])->get()->unique();
        return view('tasks.index', compact('categories'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'category_id' => 'required|exists:categories,id',
        ]);

        Task::create([
            'name' => $request->name,
            'completed' => false,
            'user_id' => Auth::user()->id,
            'due_date' => date('Y-m-d H:i:s', strtotime($request->due_date)),
            'category_id' => $request->category_id,
        ]);

        return redirect()->back();
    }

    public function edit(Task $task)
    {
        $categories = Category::all();
        return view('tasks.edit', compact('task', 'categories'));
    }

    public function update(Request $request, Task $task)
    {
        $task->name = $request->name;
        $task->due_date = date('Y-m-d H:i:s', strtotime($request->due_date));
        $task->save();

        return redirect('/tasks');
    }

    public function complete(Task $task)
    {
        $task->update([
            'completed' => true,
        ]);

        return redirect()->back();
    }

    public function destroy(Task $task)
    {
        $task->delete();

        return redirect()->back();
    }
}
