<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'role_id' => Role::where('name', Role::ROLE_ADMIN)->first()->id,
            'email_verified_at' => now(),
            'password' => '$2a$12$jKUSM6TO3hYzf/06BYIbSODltKgiIczDrerW1IjH1t0wll7Kp8gIa' //adminuser
        ]);
    }
}
