<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Users') }}
        </h2>
    </x-slot>
    <br>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="mb-3">
                        <a href="{{ route('users.create') }}" class="btn btn-primary">{{ __('Create User') }}</a>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>{{ __('Name') }}</th>
                                    <th>{{ __('Email') }}</th>
                                    <th>{{ __('Role') }}</th>
                                    <th>{{ __('Banned') }}</th>
                                    <th>{{ __('Actions') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->role->name}}</td>
                                    <td>
                                        @if ($user->banned_at)
                                            {{ $user->banned_at}}
                                        @else
                                            <i class="fas fa-times text-danger"></i>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('users.edit', $user->id) }}" title="{{ __('Edit') }}" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a>

                                        @if ($user->banned_at)
                                            <form action="{{ route('users.restore', $user) }}" method="POST" class="d-inline">
                                                @csrf
                                                @method('PUT')
                                                <button type="submit" class="btn btn-sm btn-success bg-success" title="{{ __('Restore') }}" onclick="return confirm('{{ __('Are you sure you want to restore this user?') }}')"><i class="fas fa-undo"></i></button>
                                            </form>
                                        @else
                                            <form action="{{ route('users.ban', $user) }}" method="POST" class="d-inline">
                                                @csrf
                                                @method('PUT')
                                                <button type="submit" class="btn btn-sm btn-danger bg-red-500" title="{{ __('Ban') }}" onclick="return confirm('{{ __('Are you sure you want to ban this user?') }}')"><i class="fas fa-ban"></i></button>
                                            </form>
                                        @endif

                                        <form action="{{ route('users.destroy', $user->id) }}" method="POST" class="d-inline">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-sm btn-danger bg-red-500" title="{{ __('Delete') }}" onclick="return confirm('{{ __('Are you sure you want to delete this user?') }}')"><i class="fas fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</x-app-layout>
