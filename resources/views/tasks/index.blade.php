<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Tasks') }}
        </h2>
    </x-slot>
    <br>
        <div class="container-xxl">
            <div class="row">
                <div class="col-lg-6 mx-auto">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{ route('tasks.store') }}" method="POST">
                                @csrf
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="Add Task">
                                </div>
                                <div class="form-group">
                                    <label for="category_id">Category</label>
                                    <select name="category_id" id="category_id" class="form-control">
                                        <option value="">Select a category</option>
                                        @foreach ($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="input-group date" id="datetimepicker" data-target-input="nearest">
                                        <input type="text" name="due_date" class="form-control datetimepicker-input" data-target="#datetimepicker" placeholder="Add Deadline"/>
                                        <div class="input-group-append" data-target="#datetimepicker" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary bg-primary">Add Task</button>
                            </form>
                        </div>
                    </div>
                    <div class="card mt-5" id="tasks">
                        <div class="card-body tasks-body">
                            <div class="row">
                                @foreach($categories as $category)
                                    <div class="col-md-4 mb-5">
                                        <div class="card">
                                            <div class="card-header">{{ $category->name }}</div>
                                            <ul class="list-group list-group-flush">
                                                @foreach($category->tasks as $task)
                                                    <li class="list-group-item">
                                                        <span class="{{ $task->completed ? 'text-success' : 'text-danger' }}">{{ $task->name }}</span>
                                                        <br>
                                                        <span>{{$task->due_date}}</span>
                                                        <br>
                                                        @if(!$task->completed)
                                                            <button class="btn btn-sm btn-success float-left ml-1" onclick="complete({{ $task->id }})">Complete</button>
                                                        @endif
                                                        <button class="btn btn-sm btn-danger float-left ml-1" onclick="destroy({{ $task->id }})">Delete</button>
                                                        <a href="{{ url('tasks/'.$task->id.'/edit') }}" class="btn btn-sm btn-primary float-left ml-1">Edit Task</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    @if(($loop->iteration % 3) == 0)
                                        </div><div class="row">
                                    @endif
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function complete(id) {
                location.reload(true);
                $.ajax({
                    type: 'PUT',
                    url: '/tasks/' + id + '/complete',
                    data: {
                        _token: '{{ csrf_token() }}',
                    },
                    success: function() {
                        location.reload(true);
                    }
                });
            }
            function destroy(id) {
            if(confirm('Are you sure you want to delete this Task?')) {
                setTimeout(function() {
                            location.reload();
                        }, 500);
                $.ajax({
                    type: 'DELETE',
                    url: '/tasks/' + id,
                    data: {
                        _token: '{{ csrf_token() }}',
                    },
                    success: function() {
                        setTimeout(function() {
                            location.reload();
                        }, 500);
                    }
                });
            }
            }
        </script>
</x-app-layout>
