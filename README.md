This is an ongoing hobby project. I want to create a todo-app and improve my skills while doing so.

To test the application:
- composer install
- php artisan migrate
- php artisan db:seed -> this will seed the test user which is admin@admin.com and adminuser so you can login and test the application
- php artisan serve to start the application

![image info](readme_images/tasks_page.png)
![image info](readme_images/categories_page.png)
![image info](readme_images/users_page.png)
